package com.amap.location.demo.contentprovider;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.amap.location.demo.R;

public class ProviderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider);
    }
}
