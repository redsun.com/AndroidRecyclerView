package com.hongri.recyclerview.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.hongri.recyclerview.R;
import com.hongri.recyclerview.location.dao.LocationManager;
import com.hongri.recyclerview.utils.Logger;
import com.hongri.recyclerview.webview.LoadUrlJSBridge;

/**
 * @author：zhongyao on 2016/6/29 15:52
 * @description:
 */
public class WebViewFragment extends Fragment implements LocationManager.LocationResultListener {

    private static WebViewFragment homeFragment = null;
    private WebView webView;
    private String TAG = getClass().getSimpleName();
//    private static String URL = "file:///android_asset/good.html";
    private static String URL = "file:///android_asset/index.html";
    /**
     * 单例模式--双重检查锁定
     * @return
     */
    public static WebViewFragment getInstance(){
        if (homeFragment == null){
            synchronized (WebViewFragment.class){
                if (homeFragment == null){
                    homeFragment = new WebViewFragment();
                }
            }
        }
        return homeFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocationManager.getInstance(getActivity()).initLocation(getActivity());
        LocationManager.getInstance(getActivity()).startLocation();
        LocationManager.getInstance(getActivity()).setListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_webview,container,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);

    }

    private void init(View view) {
        webView = (WebView) view.findViewById(R.id.webView);

    }

    @SuppressLint("JavascriptInterface")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Logger.d("HomeFragment--onActivityCreated()");


        webView.getSettings().setJavaScriptEnabled(true);
//        webView.addJavascriptInterface(new JSHook(), "hello");
//        webView.addJavascriptInterface(new getHtmlObject(), "jsObj");
        webView.addJavascriptInterface(loadUrlJSBridge,"urlJSBridge");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                view.loadUrl(URL);// 当打开新链接时，使用当前的 WebView，不会使用系统其他浏览器
                return true;
            }
        });

        webView.setWebChromeClient(new WebChromeClient(){

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                return super.onJsAlert(view, url, message, result);
            }

            @Override
            public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
                return super.onJsConfirm(view, url, message, result);
            }

            @Override
            public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
                return super.onJsPrompt(view, url, message, defaultValue, result);
            }
        });
    }

    @Override
    public void nextAction(String longitude, String latitude) {
        Logger.d("nextAction");
        webView.loadUrl(URL);
    }

    //    public class JSHook{
//
//        @JavascriptInterface
//        public void javaMethod(String p){
//            Logger.d("JSHook.JavaMethod() called! + "+p);
//        }
//
//        @JavascriptInterface
//        public void showAndroid(){
//            final String info = "来自手机内的内容！！！";
//            getActivity().runOnUiThread(new Runnable(){
//                @Override
//                public void run() {
//                    webView.loadUrl("javascript:show('"+info+"')");
//                }
//            });
//        }
//        public String getInfo(){
//            return "获取手机内的信息！！";
//        }
//    }

    LoadUrlJSBridge loadUrlJSBridge = new LoadUrlJSBridge(getActivity(),webView){
        @Override
        public String loadUrl(String json) {
            return super.loadUrl(json);
        }
    };

    private class getHtmlObject{

            @JavascriptInterface
            public String HtmlcallJava(){
                Logger.d("Html call Java");
                return "Html call Java";
            }

            @JavascriptInterface
            public String HtmlcallJava2(final String param){
                Logger.d("Html call Java"+param);
                return "Html call Java : " + param;

       }
           @JavascriptInterface
            public void JavacallHtml(){
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        webView.loadUrl("javascript: showFromHtml()");
                        Toast.makeText(getActivity(), "clickBtn", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @JavascriptInterface
            public void JavacallHtml2(){
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        webView.loadUrl("javascript: showFromHtml2('IT-homer blog!!!')");
                        Toast.makeText(getActivity(), "clickBtn2", Toast.LENGTH_SHORT).show();
                    }
                });
        };
    }
}
