package com.hongri.recyclerview.fragment;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hongri.recyclerview.R;
import com.hongri.recyclerview.utils.Logger;

/**
 * ContentProvider测试
 *  * zhongyao
 * 2016/2/2
 */
public class ContentProviderFragment extends Fragment {

    private static ContentProviderFragment contentProviderFragment;


    public static Fragment getInstance() {
        if (contentProviderFragment == null){
            contentProviderFragment = new ContentProviderFragment();
        }
        return contentProviderFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Logger.d("thread:"+Thread.currentThread().getName());
        Uri uri = Uri.parse("content://com.hongri.recyclerview.provider.TestContentProvider");
        getActivity().getContentResolver().query(uri,null,null,null,null);
        getActivity().getContentResolver().query(uri,null,null,null,null);
        getActivity().getContentResolver().query(uri,null,null,null,null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_content, container, false);
    }


}
