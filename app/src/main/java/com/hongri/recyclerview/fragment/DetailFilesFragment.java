package com.hongri.recyclerview.fragment;


import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hongri.recyclerview.R;
import com.hongri.recyclerview.activity.DetailActivity;
import com.hongri.recyclerview.utils.Logger;

import java.io.File;
import java.io.IOException;

/**
 * @author zhongyao 2017/5/22
 */
public class DetailFilesFragment extends Fragment {

    public static DetailFilesFragment detailFilesFragment;
    public static final String FILE_NAME = "hongri.txt";
    public static final String DIR_NAME = "yaoo";
    public static String dir = Environment.getExternalStorageDirectory().getAbsolutePath();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        File file = new File(dir,FILE_NAME);
        if (!file.exists()) {
            try {
                file.createNewFile();
                Logger.d("文件创建成功");
            } catch (IOException e) {
                e.printStackTrace();
                Logger.d("文件创建失败："+e);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail_files, container, false);
    }

    public static DetailFilesFragment getInstance(DetailActivity context) {
        if (detailFilesFragment == null) {
            detailFilesFragment = new DetailFilesFragment();
        }
        return detailFilesFragment;
    }
}
