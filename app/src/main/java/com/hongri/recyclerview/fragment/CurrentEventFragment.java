package com.hongri.recyclerview.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hongri.recyclerview.R;
import com.hongri.recyclerview.adapter.CurrentEventRecyclerAdapter;
import com.hongri.recyclerview.anim.OvershootInLeftAnimator;
import com.hongri.recyclerview.utils.DataUtil;
import com.hongri.recyclerview.utils.Logger;

/**
 * zhongyao
 * 2016/1/8
 * 互联网时事热点 Fragment
 */
public class CurrentEventFragment extends Fragment implements CurrentEventRecyclerAdapter.OnEventItemClickInterface{
    private String TAG = getClass().getSimpleName();
    private RecyclerView recyclerView;
    private CurrentEventRecyclerAdapter adapter;
    private RecyclerView.ItemAnimator animator;
    @Override
    public void onAttach(Context context) {
        Logger.d(TAG+"--onAttach");
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.d(TAG+"--onCreate");

        animator = new OvershootInLeftAnimator();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Logger.d(TAG+"--onCreateView");
        return inflater.inflate(R.layout.fragment_current_event, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new CurrentEventRecyclerAdapter(getActivity(),this,DataUtil.getCurrentEventData());

        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Logger.d(TAG+"--onActivityCreated");
    }

    @Override
    public void onStart() {
        Logger.d(TAG+"--onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Logger.d(TAG+"--onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Logger.d(TAG+"--onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Logger.d(TAG+"--onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Logger.d(TAG+"--onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Logger.d(TAG+"--onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Logger.d(TAG+"--onDetach");
        super.onDetach();
    }

    @Override
    public void OnItemClick(int position) {
        Logger.d("移除");
        recyclerView.setItemAnimator(animator);
        adapter.setData(getActivity(),this,DataUtil.getRemovedCurrentEventData(position));
        adapter.notifyItemRemoved(position);
    }
}
