package com.hongri.recyclerview.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.hongri.recyclerview.R;
import com.hongri.recyclerview.activity.DetailActivity;
import com.hongri.recyclerview.adapter.DetailPhenixRecyclerAdapter;
import com.hongri.recyclerview.utils.DataUtil;

/**
 * @author：zhongyao on 2017/7/20 18:14
 * @description:
 */
public class DetailPhenixFragment extends Fragment {
    private RecyclerView rv;
    private DetailPhenixRecyclerAdapter adapter;
    public static DetailPhenixFragment detailPhenixFragment;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail_phenix, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rv = (RecyclerView)view.findViewById(R.id.rv);
        rv.setLayoutManager(new GridLayoutManager(getActivity(),3));
        adapter = new DetailPhenixRecyclerAdapter(getActivity(), DataUtil.getImageThumbUrls());
        rv.setAdapter(adapter);
    }

    public static Fragment getInstance(DetailActivity activity) {
        if (detailPhenixFragment == null){
            detailPhenixFragment = new DetailPhenixFragment();
        }
        return detailPhenixFragment;
    }
}
