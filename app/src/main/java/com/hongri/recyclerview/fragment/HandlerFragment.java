package com.hongri.recyclerview.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hongri.recyclerview.R;
import com.hongri.recyclerview.https.MyThread;
import com.hongri.recyclerview.thread.TestHandlerThread;
import com.hongri.recyclerview.utils.Logger;

/**
 * @author：zhongyao on 2017/4/9
 * @description:Handler消息机制
 */
public class HandlerFragment extends Fragment {

    public static HandlerFragment handlerFragment;
    private TextView tv;
    private TextView tv_handlerThread;
    private TestHandlerThread handlerThread;
    private Handler mHandler;
    private boolean canShowData = false;
    private int MSG_UPDATE_INFO = 1;
    private Handler mainHandler = new Handler();
    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.arg1 == 1){
                Logger.d("主线程获取到了msg:"+msg.obj.toString());
            }
            return false;
        }
    });

    public HandlerFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_handler, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);

        initHanlder();

    }

    @Override
    public void onResume() {
        super.onResume();
        canShowData = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        canShowData = false;
    }

    private void initView(View view) {
        tv = (TextView) view.findViewById(R.id.tv);
        tv_handlerThread = (TextView) view.findViewById(R.id.tv_handlerThread);

        //另一个使用：HandlerThread的使用
        tv_handlerThread.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                handlerThread = new TestHandlerThread("handlerThread_hongri_1");
                handlerThread.start();

                mHandler = new Handler(handlerThread.getLooper()){
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);

                        handleData();
                        if (canShowData){
                            mHandler.sendEmptyMessageDelayed(MSG_UPDATE_INFO,2000);
                        }

                    }
                };
                mHandler.sendEmptyMessage(MSG_UPDATE_INFO);
            }
        });
    }

    private void handleData() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                String result = "股票实时动态:<font color='red'>%d</font>";
                result = String.format(result,(int)(Math.random()*3000 + 1000));
                tv_handlerThread.setText(Html.fromHtml(result));
            }
        });

    }

    private void initHanlder() {
        new MyThread(handler,tv).start();
    }

    public static HandlerFragment getInstance() {
        if (handlerFragment == null){
            handlerFragment = new HandlerFragment();
        }
        return handlerFragment;
    }
}
