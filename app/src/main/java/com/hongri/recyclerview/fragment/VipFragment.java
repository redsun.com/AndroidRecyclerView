package com.hongri.recyclerview.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import com.hongri.recyclerview.R;
import com.hongri.recyclerview.adapter.VipRecyclerAdapter;
import com.hongri.recyclerview.utils.DataUtil;
import com.hongri.recyclerview.utils.Logger;

/**
 * zhongyao
 * 2016/1/8
 * VVVVIP Fragment
 */
public class VipFragment extends Fragment {

    private String TAG = getClass().getSimpleName();
    private RecyclerView rv;
    private VipRecyclerAdapter adapter;
    private TextView tv;

    @Override
    public void onAttach(Context context) {
        Logger.d(TAG + "--onAttach");
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.d(TAG + "--onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Logger.d(TAG + "--onCreateView");
        return inflater.inflate(R.layout.fragment_vip, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rv = (RecyclerView)view.findViewById(R.id.rv);
        tv = (TextView)view.findViewById(R.id.tv);

        adapter = new VipRecyclerAdapter(getActivity(), DataUtil.getVIPData());
        rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv.setAdapter(adapter);

        tv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                 * 1、直接定位显示：把你所需要position的item，显示在屏幕上（item 完全暴露在屏幕上，任务即完成）
                 */
                //rv.scrollToPosition(8);

                /**
                 * 2、平滑滚动显示
                 */
                //rv.smoothScrollToPosition(8);

                /**
                 * 3、不进可以定位到指定项，还可以将该项置顶(offset==0)
                 */
                //((LinearLayoutManager)rv.getLayoutManager()).scrollToPositionWithOffset(8, 0);

                /**
                 * 4、【微信联系人字母索引定位】如果希望定位的项显示在屏幕中间，则可以计算offset，然后设置(假如屏幕高度是1920)
                 */
                //((LinearLayoutManager)rv.getLayoutManager()).scrollToPositionWithOffset(8, 1920 / 2);

                /**
                 * 5、【小说阅读器翻页效果】直接滚动一段距离(这里设置的是滚动一屏)
                 */
                rv.scrollBy(0,1920);

            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Logger.d(TAG + "--onActivityCreated");
    }

    @Override
    public void onStart() {
        Logger.d(TAG + "--onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Logger.d(TAG + "--onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Logger.d(TAG + "--onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Logger.d(TAG + "--onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Logger.d(TAG + "--onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Logger.d(TAG + "--onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Logger.d(TAG + "--onDetach");
        super.onDetach();
    }
}
