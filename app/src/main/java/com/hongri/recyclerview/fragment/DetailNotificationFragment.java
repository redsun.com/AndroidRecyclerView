package com.hongri.recyclerview.fragment;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.hongri.recyclerview.R;
import com.hongri.recyclerview.activity.DetailActivity;
import com.hongri.recyclerview.activity.SettingActivity;
import com.hongri.recyclerview.utils.Logger;

/**
 *  Notification通知Fragment
 */
public class DetailNotificationFragment extends Fragment {

    public static DetailNotificationFragment detailNotificationFragment;
    private Button btn_notification;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_detail_notification, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btn_notification = (Button) view.findViewById(R.id.btn_notification);
        btn_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationManager manager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                Intent i = new Intent(getActivity(), SettingActivity.class);
//                Intent clickIntent = new Intent(getActivity(), NotificationClickReceiver.class); //点击通知之后要发送的广播
                Notification n = new android.support.v4.app.NotificationCompat.Builder(getActivity())
                        .setContentIntent(PendingIntent.getActivity(getActivity(), 4, i, PendingIntent.FLAG_UPDATE_CURRENT))
                        .setContentText("哈哈")
                        .setSmallIcon(R.drawable.icon_360)
                        .build();

//                Notification nn = new android.support.v4.app.NotificationCompat.Builder(getActivity())
//                        .setContentIntent(PendingIntent.getBroadcast(getActivity(), 5, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT))
//                        .setContentText("哈哈!!")
//                        .setSmallIcon(R.drawable.icon_360)
//                        .build();
                showNotification();


                manager.notify(2,n);
//                manager.notify(1,nn);
            }
        });
    }


    private void showNotification() {
        // 创建一个NotificationManager的引用
        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

        Intent clickIntent = new Intent(getActivity(), NotificationClickReceiver.class); //点击通知之后要发送的广播
        int id = (int) (System.currentTimeMillis() / 1000);
        PendingIntent contentIntent = PendingIntent.getBroadcast(getActivity(), id, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(getActivity())
                .setContentIntent(contentIntent)
                .setContentTitle("哈哈哈~~~")
                .setContentText("顶顶顶顶")
                .setSmallIcon(R.drawable.ic_launcher)
                .build();
        notificationManager.notify(id, notification);
    }
    public class NotificationClickReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            Logger.d("onReceive~~~");
        }
    }

    public static Fragment getInstance(DetailActivity detailActivity) {
        if (detailNotificationFragment == null){
            detailNotificationFragment = new DetailNotificationFragment();
        }
        return detailNotificationFragment;
    }
}
