package com.hongri.recyclerview.thread;

import android.os.HandlerThread;

/**
 * Created by zhongyao on 2017/4/24.
 * HandlerThread 测试
 */

public class TestHandlerThread extends HandlerThread {
    public TestHandlerThread(String name) {
        super(name);
    }
}
