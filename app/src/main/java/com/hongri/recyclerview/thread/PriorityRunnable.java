package com.hongri.recyclerview.thread;

/**
 * Created by zhongyao on 2017/4/25.
 */

public abstract class PriorityRunnable<R> implements Runnable, Comparable<PriorityRunnable<R>> {
    private int priority;

    public PriorityRunnable(int priority) {
        if (priority < 0) {
            throw new IllegalArgumentException();
        }
        this.priority = priority;
    }

    @Override
    public int compareTo(PriorityRunnable<R> another) {
        int my = this.getPriority();
        int other = another.getPriority();
        return my < other ? 1 : my > other ? -1 : 0;
    }

    @Override
    public void run() {
        doSth();
    }

    public abstract void doSth();

    public int getPriority() {
        return priority;
    }
}
