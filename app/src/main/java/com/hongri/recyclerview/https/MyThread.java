package com.hongri.recyclerview.https;

import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

import com.hongri.recyclerview.utils.Logger;

/**
 * Created by zhongyao on 2017/4/9.
 */

public class MyThread extends Thread implements Runnable {

    private Handler handler;
    private int SUCCESS = 1;
    private TextView tv;
    public MyThread(Handler handler, TextView tv) {
        this.handler = handler;
        this.tv = tv;

    }


    @Override
    public void run() {
        super.run();

        doAction(handler);
        doPostHandlerAction(handler);
    }

    private void doPostHandlerAction(Handler handler) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                tv.setText("postHandler-workThread");
                Logger.d("postHandler-workThread");
            }
        });
    }

    private void doAction(Handler handler) {
        Message msg = Message.obtain();
        msg.obj = "我是子线程发过来的msg";
        msg.arg1 = SUCCESS;
        handler.sendMessage(msg);
    }
}
