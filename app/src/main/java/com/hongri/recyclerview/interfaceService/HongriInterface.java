package com.hongri.recyclerview.interfaceService;

import android.content.Context;

/**
 * Created by zhongyao on 2017/6/26.
 * 统一调用的接口
 */

public interface HongriInterface {
    void goTest(Context context);
}
