package com.hongri.recyclerview.interfaceService;

import android.content.Context;

import com.hongri.recyclerview.utils.ToastUtil;

/**
 * Created by zhongyao on 2017/6/26.
 */

public class HongriManager implements HongriInterface {
    private static HongriManager hongriManager;

    @Override
    public void goTest(Context context) {
        ToastUtil.ShowBottomLong(context, "goTest...");
    }

    public static synchronized HongriInterface getInstance() {
        if (hongriManager == null) {
            hongriManager = new HongriManager();
        }
        return hongriManager;
    }
}
