package com.hongri.recyclerview.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.hongri.recyclerview.bean.HotspotInfo;
import com.hongri.recyclerview.utils.ToastUtil;

/**
 * Created by zhongyao on 2018/3/13.
 */

public class HongriReceiver extends BroadcastReceiver {
    public static final String ACTION_RECEIVER = "hongri.intent.action.TEST";

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case ACTION_RECEIVER:
                HotspotInfo info = intent.getExtras().getParcelable("object");
                ToastUtil.ShowBottomShort(context, "收到广播" + info.toString());
                break;
        }
    }
}
