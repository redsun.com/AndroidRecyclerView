package com.hongri.recyclerview.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hongri.recyclerview.activity.WelcomeActivity;
import com.hongri.recyclerview.utils.Logger;

/**
 * 广播测试
 */
public class TestReceiver extends BroadcastReceiver {
    public TestReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(WelcomeActivity.broadAction)){
            Logger.d("received a message...");
        }
    }
}
