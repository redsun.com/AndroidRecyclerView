package com.hongri.recyclerview.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by zhongyao on 2017/6/16.
 */

public class HotspotInfo implements Parcelable {
    public int sign;
    public String title;
    public String url;
    public String content;

    public HotspotInfo() {
    }

    public HotspotInfo(int sign, String title, String url, String content) {
        this.sign = sign;
        this.title = title;
        this.url = url;
        this.content = content;
    }

    public static final Creator<HotspotInfo> CREATOR = new Creator<HotspotInfo>() {
        @Override
        public HotspotInfo createFromParcel(Parcel in) {
            return new HotspotInfo(in.readInt(), in.readString(), in.readString(), in.readString());
        }

        @Override
        public HotspotInfo[] newArray(int size) {
            return new HotspotInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(sign);
        dest.writeString(title);
        dest.writeString(url);
        dest.writeString(content);
    }
}
