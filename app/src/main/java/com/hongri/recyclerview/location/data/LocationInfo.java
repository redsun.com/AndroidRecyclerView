package com.hongri.recyclerview.location.data;

/**
 * Created by zhongyao on 2017/1/18.
 *
 * 高德SDK定位结果信息类
 *
 * 开发中常见错误码归类：
 *
 * 错误码4：请求服务器过程中的异常，多为网络情况差，链路不通导致
 * 错误码9：定位初始化时出现异常
 * 错误码7：KEY鉴权失败
 * 错误码13：定位失败，由于设备未开启WIFI模块或未插入SIM卡，且GPS当前不可用。
 * 错误码14：GPS 定位失败，由于设备当前 GPS 状态差。
 *
 * 详细请参考高德官方文档错误码对照表：
 * http://lbs.amap.com/api/android-location-sdk/guide/utilities/errorcode/
 */

public class LocationInfo {

    //经度
    public static String longitude;

    //纬度
    public static String latitude;

    //错误码
    public static String errorCode;

    public static void clearLocationInfo(){

        longitude = null;

        latitude = null;

        errorCode = null;
    }

}
