package com.hongri.recyclerview.location.dao;

import android.content.Context;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.hongri.recyclerview.location.data.LocationInfo;
import com.hongri.recyclerview.utils.Logger;

/**
 * Created by zhongyao on 2017/1/4.
 *
 * 高德SDK定位管理类
 */
public class LocationManager {

    private static LocationManager locationManager;
    private Context context;
    private String TAG = getClass().getSimpleName();
    private AMapLocationClient locationClient = null;
    private AMapLocationClientOption locationOption = null;

    public LocationResultListener listener;
    public void setListener(LocationResultListener listener){
        this.listener = listener;
    }
    public interface LocationResultListener{
        void nextAction(String longitude,String latitude);
    }

    public LocationManager(Context context) {
        this.context = context;
    }

    public static LocationManager getInstance(Context context) {
        if (locationManager == null) {
            locationManager = new LocationManager(context);
        }
        return locationManager;
    }

    public void initLocation(Context context) {
        //初始化client
        locationClient = new AMapLocationClient(context);
        //设置定位参数
        locationClient.setLocationOption(getDefaultOption());
        // 设置定位监听
        locationClient.setLocationListener(locationListener);
    }

    /**
     *启动定位
     * @since 2.8.0
     */
    public void startLocation() {
        // 启动定位
        if (locationClient != null){
            locationClient.startLocation();
            Logger.d(TAG + "--startLocation");
        }
    }

    /**
     * 停止定位
     *
     * @since 2.8.0
     */
    private void stopLocation() {
        // 停止定位
        if (locationClient != null){
            locationClient.stopLocation();
            Logger.d(TAG + "--stopLocation");
        }
    }

    /**
     * 销毁定位
     *
     * @since 2.8.0
     */
    private void destroyLocation() {
        if (locationClient != null) {
            /**
             * 如果AMapLocationClient是在当前Activity实例化的，
             * 在Activity的onDestroy中一定要执行AMapLocationClient的onDestroy
             */
            locationClient.onDestroy();
            locationClient = null;
            locationOption = null;
        }
        //清理掉缓存的经纬度信息，避免占用内存
        LocationInfo.clearLocationInfo();
    }

    /**
     * 定位监听
     */
    AMapLocationListener locationListener = new AMapLocationListener() {
        @Override
        public void onLocationChanged(AMapLocation location) {
            if (null != location) {
                //解析定位结果
                getLocationResult(location);
            } else {
                Logger.d(TAG + "定位失败--" + "location信息返回为null");
            }
        }
    };

    /**
     * 默认的定位参数
     *
     * @since 2.8.0
     */
    private AMapLocationClientOption getDefaultOption() {
        locationOption = new AMapLocationClientOption();
        locationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);//可选，设置定位模式，可选的模式有高精度、仅设备、仅网络。默认为高精度模式
        locationOption.setGpsFirst(false);//可选，设置是否gps优先，只在高精度模式下有效。默认关闭
        locationOption.setHttpTimeOut(30000);//可选，设置网络请求超时时间。默认为30秒。在仅设备模式下无效
        locationOption.setInterval(2000);//可选，设置定位间隔。默认为2秒
        locationOption.setNeedAddress(true);//可选，设置是否返回逆地理地址信息。默认是tre
        locationOption.setOnceLocation(true);//可选，设置是否单次定位。默认是false
        locationOption.setOnceLocationLatest(false);//可选，设置是否等待wifi刷新，默认为false.如果设置为true,会自动变为单次定位，持续定位时不要使用
        AMapLocationClientOption.setLocationProtocol(AMapLocationClientOption.AMapLocationProtocol.HTTP);//可选， 设置网络请求的协议。可选HTTP或者HTTPS。默认为HTTP
        locationOption.setSensorEnable(false);//可选，设置是否使用传感器。默认是false
        return locationOption;
    }

    /**
     * 根据定位结果返回定位信息的字符串（只解析经纬度）
     *
     * @param location
     * @return
     */
    private void getLocationResult(AMapLocation location) {

        //errCode等于0代表定位成功，其他的为定位失败，具体的可以参照官网定位错误码说明
        if (location.getErrorCode() == 0) {
            //获取经度
            LocationInfo.longitude = location.getLongitude() + "";
            //获取纬度
            LocationInfo.latitude = location.getLatitude() + "";
            //回调
            listener.nextAction(LocationInfo.longitude,LocationInfo.latitude);

            Logger.d(TAG + "定位成功--：" + "经度：" + LocationInfo.longitude + " 纬度：" + LocationInfo.latitude);
        } else {
            //定位失败错误码
            LocationInfo.errorCode = location.getErrorCode() + "";

            listener.nextAction(LocationInfo.longitude,LocationInfo.latitude);

            Logger.d(TAG + "定位失败--：" + "错误码：" + LocationInfo.errorCode);
        }
    }
}
