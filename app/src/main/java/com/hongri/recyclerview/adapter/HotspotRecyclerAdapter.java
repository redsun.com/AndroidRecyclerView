package com.hongri.recyclerview.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.hongri.recyclerview.R;
import com.hongri.recyclerview.bean.HotspotInfo;
import com.hongri.recyclerview.utils.Logger;

/**
 * Created by zhongyao on 2017/6/16.
 */
public class HotspotRecyclerAdapter extends RecyclerView.Adapter<HotspotRecyclerAdapter.HotspotHolder> {
    private static final int TITLE = 1;
    private static final int CONTENT = 2;
    private static final int CONTENT_CHANGE = 3;
    private static final int IMAGE = 4;
    private Context context;
    private HotspotListener listener;
    private ArrayList<HotspotInfo> data;
    private LayoutInflater inflater;
    private String TAG = this.getClass().getSimpleName() + "  ";

    public HotspotRecyclerAdapter(Context context, HotspotListener listener, ArrayList<HotspotInfo> data) {
        this.context = context;
        this.listener = listener;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    public void setDate(Context context, HotspotListener listener, ArrayList<HotspotInfo> data) {
        this.context = context;
        this.listener = listener;
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public HotspotHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Logger.d(TAG + "onCreateViewHolder---" + viewType);
        View view;
        if (viewType == TITLE) {
            view = inflater.inflate(R.layout.fragment_hot_spot_item, parent, false);
            return new TitleHolder(view);
        } else if (viewType == CONTENT) {
            view = inflater.inflate(R.layout.fragment_hot_spot_item_content, parent, false);
            return new ContentHolder(view);
        } else if (viewType == CONTENT_CHANGE) {
            view = inflater.inflate(R.layout.fragment_hot_spot_item_content_change, parent, false);
            return new ContentChangeHolder(view);
        } else {
            view = inflater.inflate(R.layout.fragment_hot_spot_item_image, parent, false);
            return new ImageHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(HotspotHolder holder, int position) {
        Logger.d("onBindViewHolder:" + position);
        if (holder instanceof TitleHolder) {
            ((TitleHolder)holder).title.setText("勇士夺冠");
        } else if (holder instanceof ContentHolder) {
            ((ContentHolder)holder).content.setText("勇士夺得2016-2017赛季NBA总冠军");
        } else if (holder instanceof ContentChangeHolder) {
            ((ContentChangeHolder)holder).content.setText(data.get(position).content);
        } else if (holder instanceof ImageHolder) {
            ((ImageHolder)holder).title.setText("勇士夺冠图");
            ((ImageHolder)holder).image.setImageResource(R.drawable.warrior);
        }

        Logger.d(TAG + "onBindViewHolder---" + holder.toString());
    }

    @Override
    public int getItemCount() {
        Logger.d(TAG + "size:" + data.size());
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        int sign = data.get(position).sign;
        Logger.d(TAG + "getItemViewType:" + position);
        if (sign == 0 || sign == 1) {
            Logger.d(TAG + "getItemViewType:TITLE:" + TITLE + "position:" + position);
            return TITLE;
        } else if (sign == 2 || sign == 3) {
            Logger.d(TAG + "getItemViewType:CONTENT" + CONTENT + "position:" + position);
            return CONTENT;
        } else if (sign == 4 || sign == 5) {
            Logger.d(TAG + "getItemViewType:IMAGE" + IMAGE + "position:" + position);
            return CONTENT_CHANGE;
        } else {
            return IMAGE;
        }
    }

    public class HotspotHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public HotspotHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void onClick(View v) {

        }
    }

    public class TitleHolder extends HotspotHolder {
        private TextView title;

        public TitleHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
        }
    }

    public class ContentHolder extends HotspotHolder {
        private TextView content;
        private TextView delete;

        public ContentHolder(View itemView) {
            super(itemView);
            content = (TextView)itemView.findViewById(R.id.content);
            delete = (TextView)itemView.findViewById(R.id.delete);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            switch (v.getId()) {
                case R.id.delete:
                    listener.hotspotItemRemove(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }

    public class ContentChangeHolder extends HotspotHolder {
        private TextView content;
        private TextView contentChange;

        public ContentChangeHolder(View itemView) {
            super(itemView);
            content = (TextView)itemView.findViewById(R.id.content);
            contentChange = (TextView)itemView.findViewById(R.id.change);
            contentChange.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            super.onClick(v);
            switch (v.getId()) {
                case R.id.change:
                    listener.hotspotItemChange(getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }

    public class ImageHolder extends HotspotHolder {
        private TextView title;
        private ImageView image;

        public ImageHolder(View itemView) {
            super(itemView);
            title = (TextView)itemView.findViewById(R.id.title);
            image = (ImageView)itemView.findViewById(R.id.iv);
        }
    }

    public interface HotspotListener {
        void hotspotItemRemove(int position);

        void hotspotItemChange(int position);
    }
}
