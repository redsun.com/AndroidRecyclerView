package com.hongri.recyclerview.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hongri.recyclerview.R;

import java.util.ArrayList;

/**
 * Created by zhongyao on 2017/6/15.
 */

public class CurrentEventRecyclerAdapter extends RecyclerView.Adapter<CurrentEventRecyclerAdapter.CurrentViewHolder> {
    private ArrayList<String> datas;
    private Context context;
    private LayoutInflater inflater;
    private OnEventItemClickInterface clickInterface;
    public CurrentEventRecyclerAdapter(Context context, OnEventItemClickInterface clickInterface, ArrayList<String> datas) {
        this.context = context;
        this.datas = datas;
        this.clickInterface = clickInterface;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public CurrentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_current_event_item,parent,false);
        return new CurrentViewHolder(view);
    }

    public void setData(Context context, OnEventItemClickInterface clickInterface, ArrayList<String> datas){
        this.context = context;
        this.datas = datas;
        this.clickInterface = clickInterface;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public void onBindViewHolder(CurrentViewHolder holder, int position) {
        holder.tv.setText(datas.get(position));
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public class CurrentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tv;
        private TextView remove;
        public CurrentViewHolder(View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.tv);
            remove = (TextView) itemView.findViewById(R.id.remove);
            remove.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickInterface.OnItemClick(getAdapterPosition());
        }
    }

    public interface OnEventItemClickInterface{
        void OnItemClick(int position);
    }
}
