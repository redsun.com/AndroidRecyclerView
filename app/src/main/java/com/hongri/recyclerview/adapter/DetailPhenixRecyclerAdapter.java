package com.hongri.recyclerview.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.hongri.recyclerview.R;

/**
 * Created by zhongyao on 2017/7/20.
 */

public class DetailPhenixRecyclerAdapter extends RecyclerView.Adapter<DetailPhenixRecyclerAdapter.PhenixViewHolder> {
    private Context context;
    private ArrayList<String> datas;
    private LayoutInflater inflater;

    public DetailPhenixRecyclerAdapter(Context context, ArrayList<String> datas) {
        this.context = context;
        this.datas = datas;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public DetailPhenixRecyclerAdapter.PhenixViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_detail_phenix_item, parent, false);
        view.setTag(R.id.ticket_key);
        return new PhenixViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DetailPhenixRecyclerAdapter.PhenixViewHolder holder, int position) {
        holder.iv.setImageResource(R.drawable.buffering_circle_00);

        // 取消原有的加载任务, 其中 R.id.ticket_key 须自行在项目里定义
        //if (holder.iv.getTag(R.id.ticket_key) instanceof PhenixTicket) {
        //    ((PhenixTicket)holder.iv.getTag(R.id.ticket_key)).cancel();
        //}

        // 重新绑定新的加载任务
        //PhenixTicket ticket = Phenix.instance()
        //    .load(datas.get(position))
        //    .placeholder(android.R.drawable.ic_menu_rotate) // 加载时占位图
        //    .error(android.R.drawable.ic_delete) // 异常时占位图
        //    .into(holder.iv);

        //圆角处理（无效果-缺少初始化）
        //PhenixTicket ticket = Phenix.instance()
        //    .load(datas.get(position))
        //    .placeholder(android.R.drawable.ic_menu_rotate)
        //    .error(android.R.drawable.ic_delete)
        //    .bitmapProcessors(new RoundedCornersBitmapProcessor(50, 0))
        //    .into(holder.iv);

        //高斯模糊（无效果-缺少初始化）
        //PhenixTicket ticket = Phenix.instance()
        //    .load(datas.get(position))
        //    .placeholder(android.R.drawable.ic_menu_rotate)
        //    .error(android.R.drawable.ic_delete)
        //    .bitmapProcessors(new BlurBitmapProcessor(context, 25))
        //    .into(holder.iv);

        //holder.iv.setTag(R.id.ticket_key, ticket);
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public class PhenixViewHolder extends ViewHolder {
        //private TUrlImageView tUrlImageView;
        private ImageView iv;

        public PhenixViewHolder(View itemView) {
            super(itemView);
            //tUrlImageView = (TUrlImageView)itemView.findViewById(R.id.tUrlImageView);
            iv = (ImageView)itemView.findViewById(R.id.iv);

        }
    }
}
