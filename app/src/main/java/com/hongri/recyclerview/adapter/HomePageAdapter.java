package com.hongri.recyclerview.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
/**
 * 主页ViewPager适配器
 *
 * 分析：
 * FragmentStatePagerAdapter与FragmentPagerAdapter区别：
 * FragmentPagerAdapter 每一个生成的 Fragment 都将保存在内存之中
 * （对limite之外的Fragment只会调用onDestroyView()方法，而不会销毁该Fragment），因此适用于那些相对静态的页，数量也比较少的那种；
 * FragmentStatePagerAdapter将会对limit外的page进行回收
 * （对limite之外的Fragment会调用onDestroy() 及onDetach()方法，销毁该Fragment，以达到节省资源的目的）。
 *
 * reference：http://www.open-open.com/lib/view/open1476171256890.html
 */
public class HomePageAdapter extends /*FragmentStatePagerAdapter*/FragmentPagerAdapter {

    public static final int ITEM_COUNT = 5;

//    private SparseArray<Fragment> fragments;

    private ViewPager mViewPager;
    private ArrayList<Fragment> fragments;
    public HomePageAdapter(FragmentManager fm, ViewPager viewPager, ArrayList<Fragment> fragments) {
        super(fm);
        this.mViewPager = viewPager;
        this.fragments = fragments;
//        this.fragments = new SparseArray<>();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

//    @Override
//    public Fragment getItem(int position) {
//        Fragment fragment = fragments.get(position);

//        if (fragment == null) {
//            fragment = CreateFragment(position);
//            fragments.put(position, fragment);
//        }
//
//        return fragment;
//    }

//    @Override
//    public Object instantiateItem(ViewGroup container, int position) {
//        Fragment fragment = (Fragment) super.instantiateItem(container, position);
//        if (fragments.indexOfValue(fragment) == -1) {
//            //fragments中不存在,或者是同一个类的不同对象;一般不会走到这个if分枝;现在只发现应用后台被kill,再重新启动时会
//            fragments.put(position, fragment);
//        }
//        return fragment;
//    }

    @Override
    public int getCount() {
        return fragments.size();
    }

//    @Override
//    public long getItemId(int position) {
//        return position;
//    }

//    @Override
//    public void destroyItem(ViewGroup container, int position, Object object) {
//        // don't detach
//    }

//    private Fragment CreateFragment(int position) {
//            Logger.d("createFragment-position:"+position);
//        switch (position) {
//            case 0:
//                return SettingFragement.getInstacne();
//            case 1:
//                return HomeFragment.getInstance();
//            case 2:
//                return HomeFragment.getInstance();
//            case 3:
//                return HomeFragment.getInstance();
//            case 4:
//                return SettingFragement.getInstacne();
//        }
//        return null;
//    }
}
