package com.hongri.recyclerview.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.hongri.recyclerview.R;

/**
 * @author zhongyao
 * @date 2019/1/8
 */

public class VipRecyclerAdapter extends RecyclerView.Adapter<VipRecyclerAdapter.VipViewHolder> {
    private Context context;
    private ArrayList<String> vipData;
    private LayoutInflater inflater;

    public VipRecyclerAdapter(Context context, ArrayList<String> vipData) {
        this.context = context;
        this.vipData = vipData;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public VipViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_vip_item, parent, false);
        return new VipViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VipRecyclerAdapter.VipViewHolder holder, int position) {
        holder.tv.setText(vipData.get(position));
    }

    @Override
    public int getItemCount() {
        return (vipData != null && vipData.size() > 0) ? vipData.size() : 0;
    }

    public class VipViewHolder extends ViewHolder {
        private TextView tv;

        public VipViewHolder(View view) {
            super(view);
            tv = (TextView)view.findViewById(R.id.tv);
        }
    }
}
