package com.hongri.recyclerview.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.hongri.recyclerview.R;

public class OnNewIntentTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_new_intent_test);
    }
}
