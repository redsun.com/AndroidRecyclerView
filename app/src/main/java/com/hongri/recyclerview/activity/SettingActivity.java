package com.hongri.recyclerview.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;

import com.hongri.recyclerview.R;
import com.hongri.recyclerview.fragment.SettingFragement;
import com.hongri.recyclerview.utils.Logger;

/**
 * @author：zhongyao on 2016/8/3 17:49
 * @description:
 */
public class SettingActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Logger.d("SettingActivit-->onCreate");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.settings);
        actionBar.setDisplayHomeAsUpEnabled(true);

        if(savedInstanceState == null){
            getSupportFragmentManager().beginTransaction().replace(R.id.container, SettingFragement.getInstacne()).commit();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Logger.d("SettingActivit-->onNewIntent");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.d("SettingActivit-->onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Logger.d("SettingActivit-->onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.d("SettingActivit-->onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Logger.d("SettingActivit-->onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Logger.d("SettingActivit-->onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.d("SettingActivit-->onDestroy");
    }
}
