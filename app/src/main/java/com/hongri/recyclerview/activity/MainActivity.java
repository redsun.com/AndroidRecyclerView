package com.hongri.recyclerview.activity;

import java.util.ArrayList;
import java.util.Set;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.hongri.recyclerview.R;
import com.hongri.recyclerview.adapter.HomePageAdapter;
import com.hongri.recyclerview.fragment.CurrentEventFragment;
import com.hongri.recyclerview.fragment.HomeFragment;
import com.hongri.recyclerview.fragment.HotspotFragment;
import com.hongri.recyclerview.fragment.SettingFragement;
import com.hongri.recyclerview.fragment.VipFragment;
import com.hongri.recyclerview.utils.CustomViewPager;
import com.hongri.recyclerview.utils.Logger;

//import com.crashlytics.android.Crashlytics;
//import com.crashlytics.android.ndk.CrashlyticsNdk;

//import io.fabric.sdk.android.Fabric;
/**
 * @author：zhongyao on 2016/6/30 14:32
 * @description:主界面Activity
 */
public class MainActivity extends BaseActivity implements View.OnClickListener{
    private WindowManager mWindowManager;
    private WindowManager.LayoutParams param;
    private ImageView mLayout;
    private CustomViewPager viewPager;
    private HomePageAdapter adapter;
    private ArrayList<Fragment> fragments;
    private FrameLayout layout_home,layout_hotspot,layout_vip,layout_subscribe,layout_user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        Logger.d("MainActivity--onCreate()");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String name = bundle.getString("name");
        String age = bundle.getString("age");
        Logger.d("传递过来了数据--name:"+name+" age:"+age);
        //ToastUtil.ShowBottomShort(MainActivity.this,"传递过来了数据---name:"+name+" age:"+age);

        String action = intent.getAction();
        Set<String> categories = intent.getCategories();
        Uri uri = intent.getData();
        String id = "";
        String title = "";
        if(!TextUtils.isEmpty(uri.getQuery())){
            id = uri.getQueryParameter("id");
            title = uri.getQueryParameter("title");
            //ToastUtil.ShowBottomLong(MainActivity.this,"传递过来了数据--"+"id:"+id+"title:"+title);
        }else{
            //ToastUtil.ShowBottomShort(MainActivity.this,"数据为空...");
        }
        //ToastUtil.ShowBottomShort(MainActivity.this,"传递过来了数据--"+"action:"+action+"categories:"+categories+"uri:"+uri);

        //Fabric统计初始化
//        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());

//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setTitle("红日");
//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction().replace(R.id.container, HomeFragment.getInstance()).commit();
//        }

        /**
         *Android6.0以上系统增加了权限管理，所以需要添加如下代码，来让用户选择打开桌面浮窗的权限
         */
//        if (Build.VERSION.SDK_INT >= 23) {
//            if (! Settings.canDrawOverlays(MainActivity.this)) {
//                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
//                        Uri.parse("package:" + getPackageName()));
//                startActivityForResult(intent,10);
//            }
//        }
//        showView();
        init();

    }

    private void init() {

        fragments = new ArrayList<>();
        fragments.add(new HomeFragment());
        fragments.add(new HotspotFragment());
        fragments.add(new VipFragment());
        fragments.add(new CurrentEventFragment());
        fragments.add(new SettingFragement());

        viewPager = (CustomViewPager) findViewById(R.id.viewPager);
        layout_home = (FrameLayout) findViewById(R.id.layout_home);
        layout_hotspot = (FrameLayout) findViewById(R.id.layout_hotspot);
        layout_vip = (FrameLayout) findViewById(R.id.layout_vip);
        layout_subscribe = (FrameLayout) findViewById(R.id.layout_subscribe);
        layout_user = (FrameLayout) findViewById(R.id.layout_user);

        layout_home.setOnClickListener(this);
        layout_hotspot.setOnClickListener(this);
        layout_vip.setOnClickListener(this);
        layout_subscribe.setOnClickListener(this);
        layout_user.setOnClickListener(this);

        viewPager.setPagingEnabled(true);
        viewPager.setSmoothScroll(false);
        viewPager.setOffscreenPageLimit(4);
        adapter = new HomePageAdapter(getSupportFragmentManager(),viewPager,fragments);
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPagerListener());
        viewPager.setCurrentItem(0);
        layout_home.setSelected(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_home:
                Logger.d("position:"+0);
                switchTab(0);
                break;
            case R.id.layout_hotspot:
                Logger.d("position:"+1);
                switchTab(1);
                break;
            case R.id.layout_vip:
                Logger.d("position:"+2);
                switchTab(2);
                break;
            case R.id.layout_subscribe:
                Logger.d("position:"+3);
                switchTab(3);
                break;
            case R.id.layout_user:
                Logger.d("position:"+4);
                switchTab(4);
                break;
        }
    }

    private void switchTab(int position) {
        layout_home.setSelected(false);
        layout_hotspot.setSelected(false);
        layout_vip.setSelected(false);
        layout_subscribe.setSelected(false);
        layout_user.setSelected(false);

        layout_home.setEnabled(true);
        layout_hotspot.setEnabled(true);
        layout_vip.setEnabled(true);
        layout_subscribe.setEnabled(true);
        layout_user.setEnabled(true);
        switch (position){
            case 0:
                viewPager.requestLayout();
                viewPager.setCurrentItem(0);
                layout_home.setSelected(true);
                break;
            case 1:
                viewPager.requestLayout();
                viewPager.setCurrentItem(1);
                layout_hotspot.setSelected(true);
                break;
            case 2:
                viewPager.requestLayout();
                viewPager.setCurrentItem(2);
                layout_vip.setSelected(true);
                break;
            case 3:
                viewPager.requestLayout();
                viewPager.setCurrentItem(3);
                layout_subscribe.setSelected(true);
                break;
            case 4:
                viewPager.requestLayout();
                viewPager.setCurrentItem(4);
                layout_user.setSelected(true);
                break;
        }
    }

    private class ViewPagerListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if(adapter != null) {
                for (int i = 0; i < adapter.getCount(); i++) {
                    Logger.d("位置："+position);
                    Fragment fragment = adapter.getItem(i);
                    if (fragment instanceof HomeFragment) {
                        //
                    }
                }
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }



    //    @Override
//    public void method() {
////        super.method();
//        Logger.d("main-method");
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == 10) {
//            if (Build.VERSION.SDK_INT >= 23) {
//                if (!Settings.canDrawOverlays(this)) {
//                    // SYSTEM_ALERT_WINDOW permission not granted...
//                    Toast.makeText(MainActivity.this, "not granted", Toast.LENGTH_SHORT);
//                }
//            }
//        }
//    }

//    private void showView(){
//
//        mLayout=new FloatView(getApplicationContext(),mHandler);
//
//        mLayout.setBackgroundResource(R.drawable.sun);
//
//        //获取WindowManager
//        mWindowManager=(WindowManager)getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
//        //设置LayoutParams(全局变量）相关参数
//        param = ((MyApplication)getApplication()).getMywmParams();
//
////        param.type = WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY;//展示在所有的东西之上（包括锁屏后）（be displaye on top of everything else）
//        param.type=WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;// 展示在手机中所有的应用的顶层--即系统提示层（These windows are always on top of application windows）
//        param.format=1;
//        param.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE; // 表示Window不需要获取焦点
//        param.flags = param.flags | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH;//可以监听MotionEvent的ACTION_OUTSIDE事件
//        param.flags = param.flags | WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS; // 排版限制--即允许在可见的屏幕之外
//        param.flags = param.flags | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED;
//        param.flags = param.flags | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;//保持手机屏幕亮屏
////        param.flags = param.flags | WindowManager.LayoutParams.FLAG_FULLSCREEN;
//
//        param.alpha = 1.0f;
//
//        param.gravity=Gravity.LEFT|Gravity.TOP;   //调整悬浮窗口至左上角
//        //以屏幕左上角为原点，设置x、y初始值
//        param.x=0;
//        param.y=0;
//
//        //设置悬浮窗口长宽数据
//        param.width=140;
//        param.height=140;
//
//        //显示myFloatView图像
//        mWindowManager.addView(mLayout, param);
//
//    }

//    @Override
//    public void onDestroy(){
//        super.onDestroy();
//        //在程序退出(Activity销毁）时销毁悬浮窗口
//        mWindowManager.removeView(mLayout);
//    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.d("MainActivity--onResume()");

    }

    @Override
    public void onBackPressed() {
//        Intent data = new Intent();
//        data.putExtra("like","yes");
//
//        setResult(RESULT_OK,data);

        super.onBackPressed();
    }
}
