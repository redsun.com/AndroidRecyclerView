package com.hongri.recyclerview.utils;

import com.hongri.recyclerview.thread.PausableThreadPoolExecutor;
import com.hongri.recyclerview.thread.PriorityRunnable;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by zhongyao on 2017/4/25.
 */

public class ThreadPoolUtils {
    public String TAG = ThreadPoolUtils.class.getSimpleName();
    public PausableThreadPoolExecutor executorService;

    public void executeMethod() {
        ExecutorService executorService = new ThreadPoolExecutor(3, 5, 1000, TimeUnit.MILLISECONDS, new PriorityBlockingQueue<Runnable>());
        for (int i = 0; i < 10; i++) {
            final int priority = i;
            executorService.execute(new PriorityRunnable<Runnable>(priority) {
                @Override
                public void doSth() {
                    Logger.d(TAG + "---线程：" + Thread.currentThread().getName() + "正在执行优先级为：" + priority + "的任务");
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }


    public void executePausableMethod() {
        executorService = new PausableThreadPoolExecutor(3, 5, 1000, TimeUnit.MILLISECONDS, new PriorityBlockingQueue<Runnable>());
        for (int i = 0; i < 100; i++) {
            final int priority = i;
            executorService.execute(new PriorityRunnable<Runnable>(priority) {
                @Override
                public void doSth() {
                    Logger.d(TAG + "---  线程：" + Thread.currentThread().getName() + "正在执行优先级为：" + priority + "的任务");
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void isResume() {
        executorService.resume();
    }

    public void isPause() {
        executorService.pause();
    }
}