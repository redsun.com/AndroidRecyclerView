package com.hongri.recyclerview.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * Created by zhongyao on 2018/3/1.
 */

public class SnackbarUtil {

    public static void ShowSnackbar(final Context context, View view) {
        Snackbar.make(view, "分享成功", Snackbar.LENGTH_LONG).setAction("哈哈", new OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtil.ShowBottomShort(context, "点击事件");
            }
        }).show();
    }
}
