package com.hongri.recyclerview.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.hongri.recyclerview.R;

/**
 * Created by zhongyao on 2018/2/28.
 * 自定义Dialog
 */

public class CustomDialog extends Dialog {
    private String msg = "已成功分享";
    private String msg2 = "搜索2018领取福利";
    private String msgAll = "已成功分享，快去领取福利吧";
    private String msgSpecial = "领取福利";

    public CustomDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        setContentView(R.layout.layout_custom_toast_normal);

        //initView(context);

        initView2(context);
    }

    private void initView2(final Context context) {
        ImageView iv = (ImageView)findViewById(R.id.iv);
        TextView tv = (TextView)findViewById(R.id.tv);
        TextView tv2 = (TextView)findViewById(R.id.tv2);

        iv.setImageResource(R.drawable.toast_check_icon);
        tv2.setVisibility(View.GONE);

        SpannableStringBuilder spannable = new SpannableStringBuilder(msgAll);
        Pattern pattern = Pattern.compile(msgSpecial);
        Matcher matcher = pattern.matcher(msgAll);
        while (matcher.find()) {
            spannable.setSpan(new ClickableSpan() {
                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setColor(context.getResources().getColor(R.color.red));
                    ds.setUnderlineText(false);
                }

                @Override
                public void onClick(View widget) {
                    //可以定义点击事件（区别于自定义Toast）
                    ToastUtil.ShowBottomLong(context, msgSpecial);
                }
            }, matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        tv.setText(spannable);
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        tv.setHighlightColor(Color.TRANSPARENT); //设置点击后的颜色为透明
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        getWindow().setGravity(Gravity.CENTER);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
            1200);

    }

    private void initView(final Context context) {
        ImageView iv = (ImageView)findViewById(R.id.iv);
        TextView tv = (TextView)findViewById(R.id.tv);
        TextView tv2 = (TextView)findViewById(R.id.tv2);

        iv.setImageResource(R.drawable.toast_check_icon);

        tv.setText(msg);
        tv.setTextColor(context.getResources().getColor(R.color.black));

        tv2.setText(msg2);
        tv2.setTextColor(context.getResources().getColor(R.color.blue));
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtil.ShowBottomLong(context, "点击事件");
            }
        });

        setCancelable(true);
        setCanceledOnTouchOutside(true);
        getWindow().setGravity(Gravity.CENTER);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
            1200);
    }

    /**
     * 可以控制Dialog的显示时间：
     * 效果上可以看做一个"自定义Toast"，但是可以有点击事件
     * @param time 展示的时间
     */
    public void showView(int time) {
        this.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismissView();
            }
        }, time);
    }

    public void dismissView() {
        this.dismiss();
    }

}
