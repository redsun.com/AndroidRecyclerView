package com.hongri.recyclerview.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.hongri.recyclerview.R;

/**
 * @author：zhongyao on 2016/6/30 11:41
 * @description:
 */
public class ToastUtil extends Toast {
    /**
     * Construct an empty Toast object.  You must call {@link #setView} before you
     * can call {@link #show}.
     *
     * @param context The context to use.  Usually your {@link Application}
     *                or {@link Activity} object.
     */
    public ToastUtil(Context context) {
        super(context);
    }

    public static void ShowBottomShort(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void ShowBottomShort(Context context, int id) {
        Toast.makeText(context, id, Toast.LENGTH_SHORT).show();
    }

    public static void ShowBottomLong(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * @param context
     * @param ivId        图片资源
     * @param msg         普通TEXT
     * @param msg2        修饰TEXT
     * @param msgcolor    普通TEXT的COLOR
     * @param msgcolor2   修饰TEXT的COLOR
     * @param showlength  toast显示时间
     * @param showgravity toast显示空间
     */
    public static void ShowCustomToast(final Context context, int ivId, String msg, String msg2, int msgcolor,
                                       int msgcolor2, int showlength, int showgravity) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_custom_toast_normal, null, false);
        ImageView iv = (ImageView)view.findViewById(R.id.iv);
        LinearLayout ll_layout = (LinearLayout)view.findViewById(R.id.ll_layout);

        iv.setImageResource(ivId);

        TextView tv = (TextView)ll_layout.findViewById(R.id.tv);
        TextView tv2 = (TextView)ll_layout.findViewById(R.id.tv2);
        tv.setText(msg);
        tv.setTextColor(context.getResources().getColor(msgcolor));
        tv2.setText(msg2);
        tv2.setTextColor(context.getResources().getColor(msgcolor2));
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "发生了点击效果。。。。。", Toast.LENGTH_SHORT).show();
                Logger.d("刷新。。。。");
            }
        });

        Toast toast = new Toast(context);
        toast.setDuration(showlength);
        toast.setGravity(showgravity, 0, 0);
        toast.setView(view);
        toast.show();
    }

    /**
     * 可以实现特殊字体标色，但是Toast中无法执行点击事件，也不建议执行该点击事件
     *
     * @param context
     * @param msg 整个文本
     * @param specialMsg 用颜色标注的文本
     * @param msgColor 整个文本的字体颜色
     * @param specialMsgColor 用颜色标注的文本的颜色
     */
    public static void ShowCustomToast(final Activity context, String msg, String specialMsg, int msgColor,
                                       final int specialMsgColor) {
        TextView textView = new TextView(context);
        textView.setTextColor(context.getResources().getColor(msgColor));
        textView.setBackgroundColor(context.getResources().getColor(R.color.gray_light));

        SpannableStringBuilder spannable = new SpannableStringBuilder(msg);

        Pattern pattern = Pattern.compile(specialMsg);
        Matcher matcher = pattern.matcher(msg);
        while (matcher.find()) {
            ClickableSpan what = new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    //该点击无效，同时toast用来展示和提示作用，不赞成使用toast做点击交互事件
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    //设置特殊显示文本的字体颜色
                    ds.setColor(context.getResources().getColor(specialMsgColor));
                    //设置是否有下划线
                    ds.setUnderlineText(false);
                }
            };

            spannable.setSpan(what, matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            textView.setText(spannable);
            textView.setMovementMethod(LinkMovementMethod.getInstance());
        }

        Toast toast = new Toast(context);
        toast.setDuration(LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setView(textView);
        toast.show();
    }
}
