package com.hongri.recyclerview.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 自定义ViewPager，带阻止左右滚动方法
 */
public class CustomViewPager extends android.support.v4.view.ViewPager {
    private boolean pagerEnabled;

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.pagerEnabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.pagerEnabled) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.pagerEnabled) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    public void setPagingEnabled(boolean e) {
        this.pagerEnabled = e;
    }

    private boolean smoothScroll = true;

    public void setSmoothScroll(boolean smoothScroll) {
        this.smoothScroll = smoothScroll;
    }

    @Override
    public void setCurrentItem(int item) {
        if (smoothScroll) {
            super.setCurrentItem(item);
        } else {
            super.setCurrentItem(item, false);
        }
    }
}
