package com.hongri.recyclerview.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by zhongyao on 2017/2/13.
 */

public class WebViewUtils {

    public static void launchInteractionWebView(Context context, String url, Bundle bundle) {
        if(bundle == null) {
            bundle = new Bundle();
        }

        bundle.putString("url", url);
        Intent intent = new Intent();
        intent.setClassName(context, "com.hongri.recyclerview.activity.WebViewActivity");
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

}
