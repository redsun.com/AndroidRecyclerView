package com.hongri.recyclerview.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.hongri.recyclerview.utils.Logger;

/**
 * Service测试
 */
public class TestService extends Service {

    private String TAG = getClass().getSimpleName();
    public ServiceBinder binder = new ServiceBinder();
    public TestService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.d(TAG+"---onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.d(TAG+"---onStartCommand");

        /**
         * 普通的Service中进行耗时操作的情况：
         * 1、直接在onStartCommand中开启个线程
         * 2、IntentService
         */
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        Logger.d(TAG+"---onStartCommand--finish");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.d(TAG+"---onDestroy");
    }

    /**
     * IBinder:
     * android.os.Binder@6e5de06
     * @param intent
     * @return
     */
    @Override
    public IBinder onBind(Intent intent) {
        Logger.d(TAG+"---onBind"+" binder:"+binder);
       return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Logger.d(TAG+"---onUnbind");
        return super.onUnbind(intent);
    }

    /**
     * 此方法是为了可以在Acitity中获得服务的实例
     */
    public class ServiceBinder extends Binder{
        public TestService getService(){
            return TestService.this;
        }
    }
    public void customMethod(){
        Logger.d(TAG+"---customMethod");
    }
}
