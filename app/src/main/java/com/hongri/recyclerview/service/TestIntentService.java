package com.hongri.recyclerview.service;

import android.app.IntentService;
import android.content.Intent;

import com.hongri.recyclerview.utils.Logger;

/**
 * Created by zhongyao on 2017/2/1.
 （1）  它创建了一个独立的工作线程来处理所有的通过onStartCommand()传递给服务的intents。

 （2）  创建了一个工作队列，来逐个发送intent给onHandleIntent()。

 （3）  不需要主动调用stopSelft()来结束服务。因为，在所有的intent被处理完后，系统会自动关闭服务。

 （4）  默认实现的onBind()返回null

 （5）  默认实现的onStartCommand()的目的是将intentC插入到工作队列中
 */

public class TestIntentService extends IntentService {
    private String TAG = getClass().getSimpleName();

    public TestIntentService(){
        super("TestIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.d(TAG+"---onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.d(TAG+"---onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Logger.d(TAG+"---onHandleIntent");

        String name = intent.getExtras().getString("name");
        Logger.d(TAG+" name:"+name + " thread:"+Thread.currentThread().getId());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
