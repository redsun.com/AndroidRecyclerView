package com.hongri.recyclerview.service;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import com.hongri.recyclerview.utils.Logger;

/**
 * Created by zhongyao on 2018/1/27.
 */

public class ActivityLifecycleCallbacksImpl implements ActivityLifecycleCallbacks {
    private final String TAG = getClass().getSimpleName();

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        HongriContext.pushToActivityStack(activity);
        Logger.d(TAG + ":" + "onActivityCreated" + "---" + activity + "---" + savedInstanceState);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Logger.d(TAG + ":" + "onActivityStarted" + "---" + activity);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        Logger.d(TAG + ":" + "onActivityResumed" + "---" + activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Logger.d(TAG + ":" + "onActivityPaused" + "---" + activity);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        Logger.d(TAG + ":" + "onActivityStopped" + "---" + activity);
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
        Logger.d(TAG + ":" + "onActivitySaveInstanceState" + "---" + activity + "---" + outState);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        HongriContext.popFromActivityStack(activity);
        Logger.d(TAG + ":" + "onActivityDestroyed" + "---" + activity);
    }
}
