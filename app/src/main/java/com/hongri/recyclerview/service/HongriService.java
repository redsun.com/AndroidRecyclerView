package com.hongri.recyclerview.service;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * 功能接口工厂,可用功能接口如下:
 *
 * @author zhongyao
 */
public abstract class HongriService {

    protected static HongriService instance;

    public static Context context;

    /**
     * 获得红日功能模块
     *
     * @param clazz 接口类名
     * @return T
     */
    public static @NonNull <T> T getService(Class<T> clazz) {
        return instance.getServiceImpl(clazz);
    }

    /**
     * 获得红日功能模块 是否需要提供新对象
     *
     * @param clazz           接口类名
     * @param isNeedNewObject 是否需要新的对象
     * @return T
     */
    public static @NonNull <T> T getService(Class<T> clazz, boolean isNeedNewObject) {
        return instance.getServiceImpl(clazz, isNeedNewObject);
    }

    /**
     * 获取红日功能模块
     *
     * @param clazz 接口类名
     * @return T
     */
    protected abstract @NonNull <T> T getServiceImpl(Class<T> clazz);

    /**
     * 获得红日功能模块 是否需要提供新对象
     *
     * @param clazz           接口类名
     * @param isNeedNewObject 需要新的对象
     * @return T
     */
    protected abstract @NonNull <T> T getServiceImpl(Class<T> clazz, boolean isNeedNewObject);
}
