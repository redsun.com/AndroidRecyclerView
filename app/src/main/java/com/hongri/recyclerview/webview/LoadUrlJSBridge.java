package com.hongri.recyclerview.webview;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import com.hongri.recyclerview.utils.WebViewUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhongyao on 2017/2/13.
 */

public class LoadUrlJSBridge extends SimpleJSBridge {
    private Activity mActivity;
    private WebView webView;

    public LoadUrlJSBridge(Activity mActivity, WebView webView) {
        this.mActivity = mActivity;
        this.webView = webView;
    }

    @Override
    public String loadUrl(String json) {
        try {
            JSONObject object = new JSONObject(json);
            String url = object.optString("url");
            Bundle bundle = new Bundle();
            WebViewUtils.launchInteractionWebView(mActivity,url,bundle);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return super.loadUrl(json);
    }
}
