package com.hongri.recyclerview.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import com.hongri.recyclerview.R;

/**
 * Created by zhongyao on 2016/12/5.
 */

public class APPLoading{


    public static LoadingDialog loadingDialog;

    public static void show(Context context){
        loadingDialog = new LoadingDialog(context);
        loadingDialog.show();
    }

    public static void dismiss(Context context){
        loadingDialog.dismiss();
    }

    private static class LoadingDialog extends Dialog{

       public LoadingDialog(Context context){
            super(context);

        }

        public LoadingDialog(Context context, int themeResId) {
            super(context, themeResId);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.loading);

        }
    }
}
