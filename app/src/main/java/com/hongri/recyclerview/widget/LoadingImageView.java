package com.hongri.recyclerview.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.hongri.recyclerview.R;

/**
 * TODO: document your custom view class.
 */
public class LoadingImageView extends ImageView {

    AnimationDrawable frameAnim;

    public LoadingImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
//        frameAnim = (AnimationDrawable) ResourcesCompat.getDrawable(context.getResources(), R.drawable.loading_anim, null);
//        setImageDrawable(frameAnim);
        setBackgroundResource(R.drawable.loading_anim);
        frameAnim = (AnimationDrawable) getBackground();
//        frameAnim
        startAnimation();
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility == GONE || visibility == INVISIBLE) {
            stopAnimation();
        } else {
            startAnimation();
        }
    }

    public void startAnimation() {
//        this.post(new Runnable() {

//            @Override
//            public void run() {
//				Loading.this.startAnimation(mRotateCircleAnim);

        if (frameAnim != null && !frameAnim.isRunning()) {
            frameAnim.start();
        }
//            }
//        });
    }

    public void stopAnimation() {
//        this.post(new Runnable() {
//
//            @Override
//            public void run() {
//                Loading.this.clearAnimation();

        if (frameAnim != null && frameAnim.isRunning()) {
            frameAnim.stop();
        }
//            }
//        });
    }
}